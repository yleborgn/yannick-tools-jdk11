/*
 * $Id $ yleborgn
 *
 * $Date$ 22 mars 2012
 * $Revision$
 * $Author$ Yannick Le Borgne
 *
 * Copyright CERN ${2012}, All Rights Reserved.
 */
package cern.fit;

import java.util.LinkedList;
import java.util.List;

import cern.accsoft.commons.value.Point;
import cern.accsoft.commons.value.ValueFactory;


public class FunctionGenerator {
  private List<Point> polynomeListOfPoint = new LinkedList<>();
  
  
  public FunctionGenerator(double[] polynomeCoef, double minValue, double maxValue, int nbrPoint) {
      double[] xValueCalculated = getXValueCalculated(minValue, maxValue, nbrPoint);
      for (double myValue : xValueCalculated) {
          polynomeListOfPoint.add(ValueFactory.createPoint(myValue, getPolynomeValue(polynomeCoef, myValue)));
      }
  }
  
  private double[] getXValueCalculated(double minValue, double maxValue, int nbrPoint) {
      double[] xValueCalculated = new double[nbrPoint];
      double step = (maxValue - minValue) / nbrPoint;
      for (int ix = 0; ix < nbrPoint; ix++) {
          xValueCalculated[ix] = minValue + ix * step;
      }
      return xValueCalculated;
  }
  
  private double getPolynomeValue(double[] polynomeCoef, double xValue) {
      double functionCalculated = 0;
      for (int ix = 0; ix < polynomeCoef.length; ix++) {
          functionCalculated += polynomeCoef[ix] * Math.pow(xValue, ix);
      }
      return functionCalculated;
  }
  
  public List<Point> getPolynomeListPoint() {
      return polynomeListOfPoint;
  }
  
}
