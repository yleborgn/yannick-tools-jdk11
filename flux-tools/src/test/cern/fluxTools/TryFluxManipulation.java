/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fluxTools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.gui.application.ApplicationBase;
import cern.accsoft.stream.japc.JapcFlux;
import cern.fluxTools.FluxFactory;
import cern.japc.core.Selector;
import cern.japc.core.factory.SelectorFactory;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import reactor.core.publisher.Flux;


public class TryFluxManipulation extends ApplicationBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(TryFluxManipulation.class);
    private static final String DEVICE_PROPERTY_BCT3 = "SPS.BCTDC.31832/Acquisition";
    private static final String DEVICE_PROPERTY_BLRLSS1 = "BLRSPS_LSS1/Acquisition";
    private static final Selector SELECTOR_FOR_TEST1 = SelectorFactory.newSelector("SPS.USER.SFTPRO2");
    private static final Selector SELECTOR_FOR_TEST2 = SelectorFactory.newSelector("SPS.USER.LHC1");
    private Button myButton;
    private Pane myPanel = new Pane();
    private Flux<TestObject> map;
    private JapcFlux failSafeFlux;
    private FluxFactory fluxFactoryBct;
    private FluxFactory fluxFactoryBlr;

    
    private void initFlux() {
        fluxFactoryBct = new FluxFactory(DEVICE_PROPERTY_BCT3, SELECTOR_FOR_TEST1, false);
        fluxFactoryBlr = new FluxFactory(DEVICE_PROPERTY_BLRLSS1, SELECTOR_FOR_TEST2, false);
        loadFlux();
        loadZipFluxExample();
    }
    
    private void loadFlux() {
        failSafeFlux = fluxFactoryBct.getFailSafeFlux();
        failSafeFlux.doOnNext(myFailSafeParamValue -> {
            ObjectBctForTest objectBctForTest = new ObjectBctForTest(myFailSafeParamValue);
            LOGGER.info("Dump Time Value: "+objectBctForTest.getDumpCycleTime());
            LOGGER.info("Username = "+objectBctForTest.getUserName(myFailSafeParamValue));
            LOGGER.info("Device name = "+objectBctForTest.getDeviceName(myFailSafeParamValue));
        }).subscribe();
    }
    
    private void loadZipFluxExample() {
        map = Flux.zip(fluxFactoryBct.getFailSafeFlux(), fluxFactoryBlr.getFailSafeFlux()).map(tuple -> {
            LOGGER.info("*********** Something received for Zip Flux ****************");
            return new TestObject(tuple.getT1(), tuple.getT2());
        }).doOnNext(value -> LOGGER.debug("BCT = " + value.getIntensity() + " and BLR = " + value.getBlrArray()[0]));
        map.subscribe();
    }

    //***********************************************************************************************//
    //                                  Only for having a GUI for test                               //
    //***********************************************************************************************//
    public TryFluxManipulation() {
        super("Test Flux manipulation", 1000, 600);
        myButton = new Button("Change Cycle");
        myButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
            }
        });
        myPanel.getChildren().add(myButton);
        initFlux();
    }

    @Override
    public void startApplication(Stage primaryStage) throws Exception {
        primaryStage.show();
        primaryStage.setOnCloseRequest((event) -> {
            Platform.exit();
            System.exit(0);
        });
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    protected Parent createRootNode() throws Exception {
        return myPanel;
    }
    //***********************************************************************************************//
}