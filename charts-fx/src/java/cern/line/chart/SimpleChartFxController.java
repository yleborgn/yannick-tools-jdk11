/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.line.chart;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.commons.value.Point;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.layout.BorderPane;

public class SimpleChartFxController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleChartFxController.class);
    private BorderPane chartRootPane;
    private ObservableList<XYChart.Series<Number, Number>> seriesList = FXCollections.observableArrayList();
    
    
    public SimpleChartFxController() {
        chartRootPane = new BorderPane();
        LineChart<Number,Number> lineChart = new LineChart<>(createNumberAxis(), createNumberAxis(), seriesList);
        chartRootPane.setCenter(lineChart);
    }
    
    public Node getChartPane() {
        return chartRootPane;
    }

    private NumberAxis createNumberAxis() {
        NumberAxis numberAxis = new NumberAxis();
        numberAxis.setAnimated(false);
        return numberAxis;
    }
    
    public void addSingleValueToSerie(String seriesName, double xValue, double yValue) {
        Series<Number, Number> mySerie = getSerieFromListOrCreateNewOne(seriesName);
        mySerie.getData().add(new Data<>(xValue, yValue));
        LOGGER.debug("Adding point ({},{}) to serie {}.", xValue, yValue, seriesName);
    }
    
    public void setNumberArrayToSerie(String seriesName, Number[] xValues, Number[] yValues) {
        Series<Number, Number> mySerie = getSerieFromListOrCreateNewOne(seriesName);
        ObservableList<Data<Number, Number>> observableArrayList = FXCollections.observableArrayList();
        for (int ix = 0; ix < xValues.length; ix++) {
            Data<Number, Number> dataValue = new Data<>(xValues[ix], yValues[ix]);
            observableArrayList.add(dataValue);
            LOGGER.debug("Adding point ({},{}) to serie {}.", xValues[ix], yValues[ix], seriesName);
        }
        mySerie.setData(observableArrayList);
    }
    
    public void setPointListToSerie(String seriesName, List<Point> pointList) {
        Number[] xNumber = new Number[pointList.size()];
        Number[] yNumber = new Number[pointList.size()];
        for (int ix = 0; ix < pointList.size(); ix++) {
            xNumber[ix] = pointList.get(ix).getX();
            yNumber[ix] = pointList.get(ix).getY();
        }
        setNumberArrayToSerie(seriesName, xNumber, yNumber);
    }
    
    private Series<Number, Number> getSerieFromListOrCreateNewOne(String serieName) {
        Series<Number, Number> myExistingSerie = seriesList.stream().filter(mySerie -> mySerie.getName().equals(serieName))
        .findAny().orElse(null);
        if (myExistingSerie == null) {
            myExistingSerie = new XYChart.Series<>();
            myExistingSerie.setName(serieName);
            seriesList.add(myExistingSerie);
        }
        return myExistingSerie;
    }

}
