/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fluxTools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.fluxTools.DefaultObjectFromJapcFlux;
import cern.japc.core.AcquiredParameterValue;
import cern.japc.value.MapParameterValue;


public class ObjectBctForTest implements DefaultObjectFromJapcFlux {
    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectBctForTest.class);
    private int dumpCycleTime;
    private MapParameterValue mapParameterValue;
    
    
    public ObjectBctForTest(AcquiredParameterValue bctAcqParamValue) {
        mapParameterValue = getMapParameterValue(bctAcqParamValue);
        dumpCycleTime = mapParameterValue.get("dumpCycleTime").getInt();
    }
    
    public int getDumpCycleTime() {
        return dumpCycleTime;
    }

}
