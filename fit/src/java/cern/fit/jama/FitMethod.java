/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fit.jama;

public enum FitMethod {
    JAMA("Jama"),
    APACHE_POLYNOMIAL("Apache Polynomial"),
    ;
    
    private String myFitMethod;
    
    
    private FitMethod(String fitMethod) {
        myFitMethod = fitMethod;
    }
    
    public String getFitMethod() {
        return myFitMethod;
    }

}
