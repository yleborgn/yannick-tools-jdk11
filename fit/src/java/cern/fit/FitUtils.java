/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fit;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;

import cern.accsoft.commons.value.Point;
import cern.accsoft.commons.value.ValueFactory;
import cern.fit.jama.FitMethod;
import cern.fit.jama.JamaFitterCalc;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;


public class FitUtils {
    private double[] myFitCoefCalculated;
    private Point myMinimumPoint;
    private Number minAbsc, maxAbsc;
    
    
    public FitUtils(Series<Number, Number> serieToFit, FitMethod fitMethodToUse) {
        foundExtremity(serieToFit);
        myFitCoefCalculated = getFitCoefCalculate(serieToFit, fitMethodToUse);
        myMinimumPoint = getMinimumPoint(myFitCoefCalculated[0], myFitCoefCalculated[1], myFitCoefCalculated[2]);
    }
    
    public Series<Number, Number> getFitSerie(String serieName) {
        FunctionGenerator myFunctionGenerator = new FunctionGenerator(myFitCoefCalculated, minAbsc.doubleValue(), maxAbsc.doubleValue(), 50);
        List<Point> polynomeListPoint = myFunctionGenerator.getPolynomeListPoint();
        Series<Number, Number> fitSerie = new Series<>();
        fitSerie.setName(serieName);
        polynomeListPoint.forEach(myPoint -> fitSerie.getData().addAll(getDataList(polynomeListPoint)));
        return fitSerie;
    }
    
    private double[] getFitCoefCalculate(Series<Number, Number> serieToFit, FitMethod fitMethodToUse) {
        if (fitMethodToUse.equals(FitMethod.JAMA)) {
            return getJamaFitCoefCalculate(serieToFit);
        } else if (fitMethodToUse.equals(FitMethod.APACHE_POLYNOMIAL)) {
            return getApacheFitCoefCalculate(serieToFit);
        } else {
            return null;
        }
    }
    
    private double[] getJamaFitCoefCalculate(Series<Number, Number> serieToFit) {
        List<Data<Number, Number>> myDataList = new ArrayList<>();
        serieToFit.getData().forEach(myData -> myDataList.add(myData));
        double[] myFitFunctionCoefCalculated;
        JamaFitterCalc myFitCalculation = new JamaFitterCalc(myDataList);
        myFitFunctionCoefCalculated = myFitCalculation.getCoefCalculated(3);
        return myFitFunctionCoefCalculated;
    }
    
    private double[] getApacheFitCoefCalculate(Series<Number, Number> serieToFit) {
        WeightedObservedPoints weightedObservedPoints = new WeightedObservedPoints();
        serieToFit.getData().forEach(myData -> weightedObservedPoints.add(myData.getXValue().doubleValue(), myData.getYValue().doubleValue()));
        PolynomialCurveFitter polynomialCurveFitter = PolynomialCurveFitter.create(2);
        double[] myFitCoeff = polynomialCurveFitter.fit(weightedObservedPoints.toList());
        return myFitCoeff;
    }
    
    private List<Data<Number, Number>> getDataList(List<Point> listOfPoint) {
        List<Data<Number, Number>> dataList = new ArrayList<>();
        listOfPoint.forEach(myPoint -> dataList.add(new Data<Number, Number>(myPoint.getX(), myPoint.getY())));
        return dataList;
    }
    
    /**
     * Calculate where is the minimum of the fit function.
     * 
     * @param a0 : the first order parameter of the parabolic function.
     * @param a1 : the second order parameter of the parabolic function.
     * @param a2 : the third order parameter of the parabolic function.
     * @return Point
     */
    private Point getMinimumPoint(double a0, double a1, double a2) {
        double minX, minY;
        Point minPoint;
        minX = -a1 / (2 * a2);
        minY = a0 + a1 * minX + a2 * minX * minX;
        minPoint = ValueFactory.createPoint(minX, minY);
        return minPoint;
    }
    
    private void foundExtremity(Series<Number, Number> serieToFit) {
        minAbsc = serieToFit.getData().get(0).getXValue();
        maxAbsc = serieToFit.getData().get(0).getXValue();
        serieToFit.getData().forEach(myData -> {
            if (myData.getXValue().doubleValue() < minAbsc.doubleValue()) {
                minAbsc = myData.getXValue();
            }
            if (myData.getXValue().doubleValue() > maxAbsc.doubleValue()) {
                maxAbsc = myData.getXValue();
            }
        });
        minAbsc = minAbsc.doubleValue() - 5 * minAbsc.doubleValue() /100;
        maxAbsc = maxAbsc.doubleValue() + 5 * maxAbsc.doubleValue() /100;
    }

    public double[] getFitCoefCalculated() {
        return myFitCoefCalculated;
    }

    public Point getMinimumPoint() {
        return myMinimumPoint;
    }

}
