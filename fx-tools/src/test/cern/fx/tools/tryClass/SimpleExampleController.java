/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fx.tools.tryClass;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

public final class SimpleExampleController {
    
    @FXML
    private AnchorPane myAnchorPane;

  }

