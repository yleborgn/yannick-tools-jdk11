/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.lsaFactory;

import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.commons.domain.CernAccelerator;
import cern.accsoft.commons.value.ImmutableValue;
import cern.accsoft.commons.value.Type;
import cern.accsoft.commons.value.ValueFactory;
import cern.japc.core.Selector;
import cern.japc.core.Selectors;
import cern.lsa.client.ContextService;
import cern.lsa.client.ParameterService;
import cern.lsa.client.ServiceLocator;
import cern.lsa.client.SettingService;
import cern.lsa.client.TrimService;
import cern.lsa.domain.settings.AcceleratorUser;
import cern.lsa.domain.settings.AcceleratorUsersRequest;
import cern.lsa.domain.settings.ContextSettings;
import cern.lsa.domain.settings.ContextSettingsRequest;
import cern.lsa.domain.settings.Parameter;
import cern.lsa.domain.settings.Settings;
import cern.lsa.domain.settings.StandAloneContext;
import cern.lsa.domain.settings.StandAloneCycle;
import cern.lsa.domain.settings.TrimRequest;
import cern.lsa.domain.settings.TrimResponse;
import cern.lsa.domain.settings.factory.TrimRequestBuilder;
import cern.rbac.gui.swing.RBAIntegrator;
import cern.rbac.gui.swing.RBAIntegratorContextBuilder;

public class LsaFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(LsaFactory.class);
    private CernAccelerator myCernAccelerator;

    public LsaFactory(CernAccelerator accelerator, boolean trimInTestDatabase) {
        myCernAccelerator = accelerator;
        initLsaSystemProperty(trimInTestDatabase);
    }

    private void initLsaSystemProperty(boolean trimInTestDatabase) {
        if (trimInTestDatabase) {
            System.setProperty("lsa.server", "next");
        } else {
            System.setProperty("lsa.server", myCernAccelerator.getCcsName());
        }
        System.setProperty("accelerator", myCernAccelerator.getCcsName());
        RBAIntegrator myRbaIntegrator = RBAIntegrator.getInstance();
        if (myRbaIntegrator == null) {
            RBAIntegratorContextBuilder.newInstance();
        }
    }

    public Parameter getParameter(String parameterName) {
        ParameterService parameterService = ServiceLocator.getService(ParameterService.class);
        Parameter findParameterByName = parameterService.findParameterByName(parameterName);
        LOGGER.debug("Parameter found: " + findParameterByName);
        return findParameterByName;
    }

    public ImmutableValue getSettingValue(String parameterName, Selector selector) {
        Parameter myParameter = getParameter(parameterName);
        ContextSettings myContextSettings = getContextSettings(myParameter, selector);
        ImmutableValue parameterValue = Settings.getValue(myContextSettings, myParameter);
        LOGGER.debug("For parameter " + myParameter.getName() + " , value = " + parameterValue.toString());
        return parameterValue;
    }

    public Type getParameterValueType(String parameterName) {
        Parameter myParameter = getParameter(parameterName);
        Type valueType = myParameter.getValueType();
        return valueType;
    }
    
    /**
     * Get the {@link CernAccelerator} by passing the {@link Selector} in argument.
     * @param selector {@link Selector}
     * @return {@link CernAccelerator}
     */
    public static CernAccelerator getCernAcceleratorBySelector(Selector selector) {
        String timingDomain = Selectors.extractTimingDomain(selector);
        return CernAccelerator.byCcsName(timingDomain);
    }

    private ContextSettings getContextSettings(Parameter parameter, Selector selector) {
        Collection<Parameter> parameterList = Arrays.asList(parameter);
        SettingService settingService = ServiceLocator.getService(SettingService.class);
        ContextSettings myContextSettings = settingService.findContextSettings(
                ContextSettingsRequest.byStandAloneContextAndParameters(getStandAloneContext(selector), parameterList));
        return myContextSettings;
    }

    // TODO ne semble plus fonctiopnner, avec la nouvelle implementation comme la nouvelle. A verifier quand le timing sera plus stable.
    private StandAloneContext getStandAloneContext(Selector selectorToUse) {
        ContextService myContextService = ServiceLocator.getService(ContextService.class);
        AcceleratorUsersRequest byUserName = AcceleratorUsersRequest.byUserName(selectorToUse.getId());
        AcceleratorUser acceleratorUser = myContextService.findAcceleratorUser(byUserName);
        StandAloneContext findStandAloneContextByAcceleratorUser = myContextService.findStandAloneContextByAcceleratorUser(acceleratorUser);
        return findStandAloneContextByAcceleratorUser;
//        StandAloneContext findStandAloneContextByUser = myContextService.findStandAloneContextByUser(selectorToUse.getId());
//        return findStandAloneContextByUser;
    }

    private StandAloneCycle getStandAloneCycle(Selector selectorToUse) {
        StandAloneCycle myStandaloneCycle = (StandAloneCycle) getStandAloneContext(selectorToUse);
        LOGGER.debug("Standalone Cycle for " + selectorToUse.getId() + " = " + myStandaloneCycle);
        return myStandaloneCycle;
    }

    public void trimParameter(String parameterName, Selector selector, ImmutableValue valueToTrim,
            String trimDescription) throws Exception {
        StandAloneCycle standAloneCycle = getStandAloneCycle(selector);
        Parameter myParameter = getParameter(parameterName);
        TrimRequestBuilder trimRequestBuilder = new TrimRequestBuilder().setDescription(trimDescription)
                .setContext(standAloneCycle);
        if (valueToTrim.getType().isScalar()) {
            trimRequestBuilder.addScalar(myParameter, ValueFactory.createScalar(valueToTrim));
        } else if (valueToTrim.getType().isFunction()) {
            trimRequestBuilder.addFunction(myParameter, ValueFactory.createFunction(valueToTrim));
        } else {
            LOGGER.error("Value type " + valueToTrim.getType().getName() + " not compatible with the trim method.");
        }
        TrimRequest myTrimRequest = trimRequestBuilder.build();
        TrimService myTrimService = ServiceLocator.getService(TrimService.class);
        TrimResponse trimResponse = myTrimService.trimSettings(myTrimRequest);
        if (trimResponse.isDrivePerformed()) {
            LOGGER.info("Trim sent on " + parameterName + ". New value = " + valueToTrim);
        }
    }

}
