/*
 * $Id $ yleborgn
 *
 * $Date$ 21 juin 2012
 * $Revision$
 * $Author$ Yannick Le Borgne
 *
 * Copyright CERN ${2012}, All Rights Reserved.
 */
package cern.fit.jama;

import java.util.List;

import javafx.scene.chart.XYChart.Data;


public class JamaFitterCalc {
  private double[] xValueArray, yValueArray;
  
  public JamaFitterCalc(double[] xValue, double[] yValue) {
    xValueArray = xValue;
    yValueArray = yValue;
  }
  
  public JamaFitterCalc(List<Data<Number, Number>> dataList) {
      xValueArray = new double[dataList.size()];
      yValueArray = new double[dataList.size()];
      for (int ix = 0; ix < dataList.size(); ix++) {
          xValueArray[ix] = dataList.get(ix).getXValue().doubleValue();
          yValueArray[ix] = dataList.get(ix).getYValue().doubleValue();
      }
  }
  
  private Matrix generateMatrixY(int nbArgument) {
    double[] valY = new double[nbArgument];
    for (int ix = 0; ix < nbArgument; ix++) {
      double sommeValue = 0;
      for (int iy = 0; iy < yValueArray.length; iy++) {
        sommeValue += Math.pow(xValueArray[iy], ix) * yValueArray[iy];
      }
      valY[ix] = sommeValue;
    }
    Matrix myMatrixY = new Matrix(valY, nbArgument);
    return myMatrixY;
  }
  
  private Matrix getFitMatrix(int nbArgument) {
    double[][] fitMatrix = new double[nbArgument][nbArgument];
    for (int ix = 0; ix < nbArgument; ix++) { // On balaye les colonnes
      for (int iy = 0; iy < nbArgument; iy++) { // On balaye les lignes
        double sommeValue = 0;
        for (int iz = 0; iz < xValueArray.length; iz++) {
          sommeValue += Math.pow(xValueArray[iz], ix + iy);
        }
        fitMatrix[iy][ix] = sommeValue;
      }
    }
    Matrix myFitMatrix = new Matrix(fitMatrix);
    return myFitMatrix;
  }
  
  /**
   * Calculate the fit coefficient and return them.
   * @param nbArgument - int : number of coefficient we want to calculate.
   * @return double[]
   */
  public double[] getCoefCalculated(int nbArgument) {
    double[] coefCalculated = new double[nbArgument];
    Matrix myMatrixY = generateMatrixY(nbArgument);
    Matrix myFitMatrix = getFitMatrix(nbArgument);
    Matrix myInverseMatrix = myFitMatrix.inverse();
    Matrix myCoefMatrix = myInverseMatrix.times(myMatrixY);
    for (int ix = 0; ix < nbArgument; ix++) {
      coefCalculated[ix] = myCoefMatrix.get(ix, 0);
    }
    return coefCalculated;
  }
  
}
