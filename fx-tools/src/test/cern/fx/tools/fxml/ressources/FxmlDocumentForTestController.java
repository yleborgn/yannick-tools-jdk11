/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fx.tools.fxml.ressources;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class FxmlDocumentForTestController {
    @FXML
    private AnchorPane myRootAnchorPaneForTest;
    @FXML
    private Label labelForTest;
    
    public String getLabelText() {
        return labelForTest.getText();
    }

}
