package cern.fluxTools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.gui.application.ApplicationBase;
import cern.accsoft.stream.japc.JapcFlux;
import cern.japc.core.FailSafeParameterValue;
import cern.japc.core.Selector;
import cern.japc.core.factory.SelectorFactory;
import cern.japc.value.MapParameterValue;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


public class TryFluxFactory extends ApplicationBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(TryFluxFactory.class);
    private static final String DEVICE_PROPERTY_NAME_FOR_TEST = "SPS.BCTDC.31832/Acquisition";
    private static final Selector SELECTOR_FOR_TEST = SelectorFactory.newSelector("SPS.USER.SFTPRO2");
    private Pane myPanel = new Pane();
    private Button myButton;
    private FluxFactory bctFluxFactory;
    
    
    private void loadFluxSouscription() {
        bctFluxFactory = new FluxFactory(DEVICE_PROPERTY_NAME_FOR_TEST, SELECTOR_FOR_TEST, false);
        showFluxData();
        showFluxFirstData();
    }
    
    private void showFluxFirstData() {
        FluxFactory fluxFactory = new FluxFactory("BA3InjPhaseShifter/PhaseSettings", SELECTOR_FOR_TEST, true);
        JapcFlux failSafeFlux = fluxFactory.getFailSafeFlux();
        failSafeFlux.doOnNext(myFailSafe -> LOGGER.info("New RF data received: "+myFailSafe.getParameterName())).subscribe();
    }
    
    private void showFluxData() {
        JapcFlux failSafeFlux = bctFluxFactory.getFailSafeFlux();
        failSafeFlux.doOnNext(failSafeValue -> displayData(failSafeValue))
                    .doOnError(onErrorOnFlux -> LOGGER.error("Error on Flux: "+onErrorOnFlux))
                    .subscribe();
    }
    
    private void displayData(FailSafeParameterValue fsValue) {
        MapParameterValue myMapParameterValue = (MapParameterValue) fsValue.getValue();
        float intensity = myMapParameterValue.get("dumpInt").getFloat();
        LOGGER.info("Flux Value received = "+intensity);
    }
    
    //***********************************************************************************************//
    //                                  Only for having a GUI for test                               //
    //***********************************************************************************************//
    public TryFluxFactory() {
        super("Test BCT Acquisition", 1000, 600);
        myButton = new Button("Change Cycle");
        myButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                bctFluxFactory.stopFlux();
                bctFluxFactory = new FluxFactory(DEVICE_PROPERTY_NAME_FOR_TEST, SelectorFactory.newSelector("SPS.USER.AWAKE1"), false);
                showFluxData();
            }
        });
        myPanel.getChildren().add(myButton);
        loadFluxSouscription();
    }
    

	@Override
	public void startApplication(Stage primaryStage) throws Exception {
	    primaryStage.show();
        primaryStage.setOnCloseRequest((event) -> {
            Platform.exit();
            System.exit(0);
        });
	}

	public static void main(String[] args) {
		launch(args);
	}

    @Override
    protected Parent createRootNode() throws Exception {
        return myPanel;
    }
    //***********************************************************************************************//
}
