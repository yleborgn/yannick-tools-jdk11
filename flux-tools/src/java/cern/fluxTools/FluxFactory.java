/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fluxTools;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.stream.japc.JapcFlux;
import cern.accsoft.stream.japc.JapcFluxBuilder;
import cern.japc.core.Selector;


public class FluxFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(FluxFactory.class);
    private String devicePropertydUrl;
    private JapcFlux myFailSafeFlux;

    
    @Deprecated
    public FluxFactory(String devicePropertyUrl, Selector selector) {
        this.devicePropertydUrl = devicePropertyUrl;
        myFailSafeFlux = getJapcFlux(selector, devicePropertyUrl);
    }
    
    public FluxFactory(String devicePropertyUrl, Selector selector, boolean isFirstUpdateNeeded) {
        this.devicePropertydUrl = devicePropertyUrl;
        if (isFirstUpdateNeeded) {
            myFailSafeFlux = getJapcFlux(selector, devicePropertyUrl);
        } else {
            myFailSafeFlux = getJapcFlux(selector, devicePropertyUrl).filterFirstUpdate();
        }
        LOGGER.info("Flux started on "+devicePropertyUrl +" on the user "+selector.getId());
    }
    
    private JapcFlux getJapcFlux(Selector selector, String devicePropertyUrl) {
        JapcFlux myJapcFlux = JapcFluxBuilder.fromParameterName(devicePropertyUrl)
            .selector(selector)
            .restartDelayOnException(Duration.ofSeconds(10))
            .build();
        return myJapcFlux;
    }
    
    public void stopFlux() {
        myFailSafeFlux.stopSubscription();
        LOGGER.info("Flux stopped on "+devicePropertydUrl +".");
    }
    
    public JapcFlux getFailSafeFlux() {
        return myFailSafeFlux;
    }

}
