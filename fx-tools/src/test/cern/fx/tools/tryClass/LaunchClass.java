/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fx.tools.tryClass;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public final class LaunchClass extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        final Node root = loadFxml("SimpleExample.fxml");
        final Scene scene = new Scene((Parent) root, 300, 250);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Test FXML");
        primaryStage.show();
    }

    private Node loadFxml(String fxmlFileName) {
        FXMLLoader fxmlLoader = new FXMLLoader(SimpleExampleController.class.getResource(fxmlFileName));
        try {
            Node root = (Node) fxmlLoader.load();
            return root;
        } catch (IOException ioe) {
            System.err.println("Error loading fxml file: " + ioe.toString());
            return null;
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

}
