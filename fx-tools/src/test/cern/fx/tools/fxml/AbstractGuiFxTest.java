/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fx.tools.fxml;

import org.testfx.framework.junit.ApplicationTest;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public abstract class AbstractGuiFxTest extends ApplicationTest {
    
    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(getRootNode(), 500, 500);
        stage.setScene(scene);
        stage.show();
    }
    
    protected abstract Parent getRootNode();

}
