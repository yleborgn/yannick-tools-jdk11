/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fx.tools.tryClass;

import cern.fx.tools.fxml.FxmlTools;
import cern.fx.tools.fxml.ressources.FxmlDocumentForTestController;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TryFxmlDocumentForTest extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        final Node root = loadFxml();
        final Scene scene = new Scene((Parent) root, 300, 250);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Test FXML");
        primaryStage.show();
    }

    private Node loadFxml() {
        FxmlTools fxmlTools = new FxmlTools(FxmlDocumentForTestController.class);
        Node root = fxmlTools.getFxmlRootNode();
        return root;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
