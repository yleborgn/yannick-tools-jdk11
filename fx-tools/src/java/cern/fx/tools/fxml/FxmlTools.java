package cern.fx.tools.fxml;

/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

import static java.util.Objects.requireNonNull;
import static javafx.fxml.FXMLLoader.CONTROLLER_SUFFIX;

import java.io.IOException;

import org.assertj.core.util.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

/**
 * Class that help loading fmxl file.
 * 
 * @author yleborgn
 */
public class FxmlTools {
    private static final Logger LOGGER = LoggerFactory.getLogger(FxmlTools.class);
    private FXMLLoader fxmlLoader;

    /**
     * This is the construction of this class. The controller class should be passed, and the fxml file is loaded if the
     * name is the same, without the suffice controller.
     * 
     * @param class of the javaFX controller class.
     */
    public FxmlTools(Class<?> controllerClass) {
        requireNonNull(controllerClass, "Controller class must not be null");
        fxmlLoader = loadAndGetFxml(controllerClass);
    }

    private static FXMLLoader loadAndGetFxml(Class<?> controllerClass) {
        return new FXMLLoader(controllerClass.getResource(getFxmlFileNameFormController(controllerClass)));
    }

    @VisibleForTesting
    static String getSimpleNameForController(Class<?> controllerClass) {
        return controllerClass.getSimpleName().replaceAll(CONTROLLER_SUFFIX + "$", "");
    }
    
    @VisibleForTesting
    static final String getFxmlFileNameFormController(Class<?> controllerClass) {
        String simpleNameForController = getSimpleNameForController(controllerClass);
        return simpleNameForController +".fxml";
    }

    /**
     * Get the root node defined in the fxml file.
     * @return {@link Node}
     */
    public Node getFxmlRootNode() {
        try {
            return fxmlLoader.load();
        } catch (IOException ioe) {
            LOGGER.error("Error when loading root Node from fxml file.", ioe);
            return null;
        }
    }

    /**
     * Get the controller instance class defined in the fxml file.
     * @return the controller class.
     */
    public <C> C getFxmlController() {
        return fxmlLoader.getController();
    }

}
