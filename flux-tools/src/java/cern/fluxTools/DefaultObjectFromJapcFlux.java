/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fluxTools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.japc.core.AcquiredParameterValue;
import cern.japc.core.ParameterException;
import cern.japc.core.factory.ParameterFactory;
import cern.japc.core.transaction.TransactionalParameter;
import cern.japc.value.MapParameterValue;


public interface DefaultObjectFromJapcFlux {

    public default String getDeviceName(AcquiredParameterValue acqParamValue) {
        try {
            TransactionalParameter myParameter = ParameterFactory.newInstance()
                    .newParameter(acqParamValue.getParameterName());
            return myParameter.getDeviceName();
        } catch (ParameterException pe) {
            MY_LOGGER.LOGGER.error("Problem when creating the parameter with the name:" +acqParamValue.getParameterName(), pe.getCause());
            return null;
        }
    }

    public default String getParameterName(AcquiredParameterValue acqParamValue) {
        return acqParamValue.getParameterName();
    }

    public default long getCycleStamp(AcquiredParameterValue acqParamValue) {
        return acqParamValue.getHeader().getCycleStamp();
    }

    public default long getAcqStamp(AcquiredParameterValue acqParamValue) {
        return acqParamValue.getHeader().getAcqStamp();
    }
    
    public default String getUserName(AcquiredParameterValue acqParamValue) {
        return acqParamValue.getHeader().getSelector().toString();
    }
    
    public default MapParameterValue getMapParameterValue(AcquiredParameterValue acqParamValue) {
        return (MapParameterValue) acqParamValue.getValue();
    }
    
    public final class MY_LOGGER {
        private static final Logger LOGGER = LoggerFactory.getLogger(DefaultObjectFromJapcFlux.class);
    }

}
