# yannick-tools for jdk11

This is a multi-project structure. Here a re the projects inside:
 - **standard line chart**: a chart in OpenJFX 11, with basic features.
 - **fit**: different kind of fit that can be used with charts in javaFX.
 - **flux-tools**: some basic classes to facilitate flux manipulation.
 - **fx-tools**: library that help loading fxml and css file with controller.
