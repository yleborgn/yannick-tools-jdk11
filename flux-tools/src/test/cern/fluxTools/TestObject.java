/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fluxTools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.japc.core.FailSafeParameterValue;
import cern.japc.value.MapParameterValue;


public class TestObject {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestObject.class);
    private float[] blrArray;
    private float intensity;
    
    public TestObject(FailSafeParameterValue bctFspv, FailSafeParameterValue blrFspv) {
        loadBct(bctFspv);
        loadBlr(blrFspv);
        LOGGER.debug("New TestObject generated.");
    }
    
    private void loadBct(FailSafeParameterValue bctFspv) {
        MapParameterValue myMapParameterValue = (MapParameterValue) bctFspv.getValue();
        intensity = myMapParameterValue.get("dumpInt").getFloat();
        LOGGER.debug("Flux Value received on "+bctFspv.getParameterName() +" = "+intensity);
    }
    
    private void loadBlr(FailSafeParameterValue blrFspv) {
        MapParameterValue myMapParameterValue = (MapParameterValue) blrFspv.getValue();
        blrArray = myMapParameterValue.get("calThresholds").getFloats();
        LOGGER.debug("Flux Value received on "+blrFspv.getParameterName() +" = "+blrArray[0]);
    }

    public float[] getBlrArray() {
        return blrArray;
    }

    public float getIntensity() {
        return intensity;
    }

}
