/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.line.chart;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import cern.accsoft.commons.value.Point;
import cern.accsoft.commons.value.ValueFactory;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TrySimpleChartFxController extends Application {

    private SimpleChartFxController simpleChartFxController;

    @Override
    public void start(Stage primaryStage) throws Exception {
        simpleChartFxController = new SimpleChartFxController();
        Scene scene = new Scene((Parent) simpleChartFxController.getChartPane());
        primaryStage.setScene(scene);
        primaryStage.setTitle("Test Simple chart FX with OpenJdk 11");
        primaryStage.show();
        loadNewFullData();
    }

    private void loadNewFullData() {
        ScheduledExecutorService myExecutorService = Executors.newSingleThreadScheduledExecutor();
        myExecutorService.scheduleAtFixedRate(() -> {
            List<Point> randomListOfPoint = getRandomListOfPoint(10);
            Platform.runLater(() -> simpleChartFxController.setPointListToSerie("Main series", randomListOfPoint));
            List<Point> randomListOfPointForOverlay = getRandomListOfPoint(20);
            Platform.runLater(() -> simpleChartFxController.setPointListToSerie("Overlay Example Chart",
                    randomListOfPointForOverlay));
        }, 0, 2, TimeUnit.SECONDS);
    }

    private List<Point> getRandomListOfPoint(int nbrOfPoint) {
        List<Point> myListOfPoint = new ArrayList<>();
        for (int ix = 0; ix < nbrOfPoint; ix++) {
            Point myPoint = ValueFactory.createPoint(ix, getRandomDoubleValue());
            myListOfPoint.add(myPoint);
        }
        return myListOfPoint;
    }

    private double getRandomDoubleValue() {
        Random myRandom = new Random();
        int myIntRandomValue = getIntRandomValue();
        double myDoubleRandom = myRandom.nextDouble();
        double myRandomValue = myIntRandomValue * myDoubleRandom;
        return myRandomValue;
    }

    private int getIntRandomValue() {
        Random myRandom = new Random();
        int myIntRandomValue = myRandom.nextInt(101);
        return myIntRandomValue;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

}
