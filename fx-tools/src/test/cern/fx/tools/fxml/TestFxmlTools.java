/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fx.tools.fxml;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

import org.junit.Test;

import cern.fx.tools.fxml.ressources.FxmlDocumentForTestController;


public class TestFxmlTools {
    
    @Test()
    public void testFxmlToolsWithFxmlFileNull() {
        @SuppressWarnings("unused")
        Exception exception = assertThrows(NullPointerException.class, () -> {
            new FxmlTools(null);
        });
        assertThat(exception.getMessage()).isEqualTo("Controller class must not be null");
    }
    
    @Test
    public void testGetFxmlSimpleNameForController() {
        String fxmlSimpleNameForController = FxmlTools.getSimpleNameForController(FxmlDocumentForTestController.class);
        assertThat(fxmlSimpleNameForController).isEqualTo("FxmlDocumentForTest");
    }
    
    @Test
    public void testGetFxmlFileNameFormController() {
        String fxmlFileNameFormController = FxmlTools.getFxmlFileNameFormController(FxmlDocumentForTestController.class);
        assertThat(fxmlFileNameFormController).isEqualTo("FxmlDocumentForTest.fxml");
    }
    
//    @Test
//    public void testGetFxmlRootNode() {
//        FxmlTools fxmlTools = new FxmlTools(FxmlDocumentForTestController.class);
//        Node fxmlRootNode = fxmlTools.getFxmlRootNode();
//        
//        assertThat(fxmlRootNode).isInstanceOf(AnchorPane.class);
//    }
    
//    @Test
//    public void testGetFxmlController() {
//        FxmlTools fxmlTools = new FxmlTools(FxmlDocumentForTestController.class);
//        Object fxmlController = fxmlTools.getFxmlController();
//        assertThat(fxmlController).isInstanceOf(FxmlDocumentForTestController.class);
//    }

}
