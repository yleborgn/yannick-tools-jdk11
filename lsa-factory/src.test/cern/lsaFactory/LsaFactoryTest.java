/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.lsaFactory;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import cern.accsoft.commons.domain.CernAccelerator;
import cern.japc.core.Selector;
import cern.japc.core.factory.SelectorFactory;

public class LsaFactoryTest {

    @Test
    public void testGettingCernAcceleratorBySelector() {
        Selector selectorForTest = SelectorFactory.newSelector("SPS.USER.SFTPRO2");
        CernAccelerator cernAcceleratorBySelector = LsaFactory.getCernAcceleratorBySelector(selectorForTest);
        assertThat(cernAcceleratorBySelector).isEqualTo(CernAccelerator.SPS);
        assertThat(cernAcceleratorBySelector).isNotEqualTo(CernAccelerator.PS);
    }
    
}
